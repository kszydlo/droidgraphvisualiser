package com.example.dgvrunnerdemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
	DGVImporter parser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void loadFileAction(View view) throws IOException {
		EditText field = (EditText)findViewById(R.id.editText1);
		StringBuilder contents = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(new File(field.getText().toString())));
		
		String text = "";
		while (text != null) {
			contents.append(text);
			text = reader.readLine();
		}
		reader.close();
		
		parser = new DGVImporter(contents.toString());
	}
	
	public void displayAction(View view) {
		if (parser == null) {
			System.err.println("No data.");
		} else {
			parser.display();
		}
	}

}
