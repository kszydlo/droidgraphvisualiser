package com.droidgraphvisualiser.parser;

import com.droidgraphvisualiser.parser.impl.ParserImpl;

/**
 * @author Jamnic
 * Singleton, the only task of this class is to provide Modeler with instance of IParser.
 */
public final class Parser {
	private static IParser parser;
	private Parser() { }
	
	/**
	 * Returns one and the only instance of IParser.
	 * @return Returns one and the only instance of IParser.
	 */
	public static IParser getInstance() {
		if ( parser == null ) parser = new ParserImpl();
		return parser;
	}
}
