package com.droidgraphvisualiser.parser.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;

import com.droidgraphvisualiser.modeler.structures.Edge;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Link;
import com.droidgraphvisualiser.modeler.structures.Vertex;

/**
 * @author Jamnic
 * Class that exports set of Graph objects (Set<Graph>) to the .viz file.
 */
public class Exporter {
	/**
	 * Builds a String that includes parsed single Graph object and
	 * its Vertexes and Edges in .viz format.
	 * @param graph
	 * @throws FileNotFoundException 
	 */
	public static void export(Graph graph, File file) throws FileNotFoundException {
		StringBuilder stringBuilder = new StringBuilder();
		if ( graph.isDirected() )	stringBuilder.append("digraph ");
		else stringBuilder.append("graph ");
		stringBuilder.append(graph.getLabel() + " {\n");

		String text;
		for ( Vertex vertex : graph.getVertexes() )
			if ( (text = export(vertex)) != null ) stringBuilder.append("\n\t" + text);

		stringBuilder.append("\n");
	
		for ( Link link : graph.getLinks() )
			stringBuilder.append(export(link, graph));
		
		stringBuilder.append("\n}");
		
		PrintWriter writer = new PrintWriter(file);
		writer.write(stringBuilder.toString());
		writer.close();
	}

	/**
	 * Exports Link object into Edges objects with .viz format.
	 * @param link
	 * @param graph
	 * @return String
	 */
	private static String export(Link link, Graph graph) {
		StringBuilder stringBuilder = new StringBuilder();
		for ( Edge edge : link.getEdges() )
			stringBuilder.append("\n\t" + export(edge, link.getStartpoint(), link.getEndpoint(), graph.isDirected()));
		return stringBuilder.length() == 0 ? null : stringBuilder.toString();
	}
	
	/**
	 * Parses Edge object into .viz format.
	 * @param edge - Edge object to parse.
	 * @return returns .viz format of this Edge.
	 * @throws ObjectNotLabelledException
	 */
	private static String export(Edge edge, Vertex firstVertex, Vertex secondVertex, boolean isDirected) {
		StringBuilder stringBuilder = new StringBuilder(firstVertex.getLabel());
		if ( isDirected ) stringBuilder.append(" -> ");
		else stringBuilder.append(" -- ");
		stringBuilder.append(secondVertex.getLabel());
		stringBuilder.append(export(edge.getAttributes()));

		return stringBuilder.length() == 0 ? null : stringBuilder.toString();
	}
	
	/**
	 * Parses Vertex object into .viz format unless it has attributes.
	 * @param vertex - Vertex object to parse.
	 * @return returns String representation of .viz format of this Vertex, or null, if Vertex does not have attributes.
	 * @throws ObjectNotLabelledException - happens, when Vertex does not have its Label attribute.
	 */
	private static String export(Vertex vertex) {
		StringBuilder stringBuilder = new StringBuilder();
		
		Map<String, Object> attributes = vertex.getAttributes();
		if ( attributes != null && attributes.size() != 0 ) {
			stringBuilder.append(vertex.getLabel());
			stringBuilder.append(export(attributes));
		}

		return stringBuilder.length() == 0 ? null : stringBuilder.toString();
	}
	
	/**
	 * Auxiliary method to the convert(Edge) and convert(Vertex) methods. Converts set of 
	 * attributes into .viz format.
	 * @param attributes
	 * @return String
	 */
	private static String export(Map<String, Object> attributes) {
		StringBuilder stringBuilder = new StringBuilder();
		Iterator<String> iterator = attributes.keySet().iterator();
		String key = iterator.next();
		stringBuilder.append(" [" + key + "=" + attributes.get(key));
		while ( iterator.hasNext() ) {
			key = iterator.next();
			stringBuilder.append("," + key + "=" + attributes.get(key));
		}
		stringBuilder.append("];");
		return stringBuilder.toString();
	}
}