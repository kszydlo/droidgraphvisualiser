package com.droidgraphvisualiser.parser.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;
import java.util.List;

import com.alexmerz.graphviz.ParseException;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.parser.IParser;
import com.droidgraphvisualiser.parser.exceptions.ExportFailedException;
import com.droidgraphvisualiser.parser.exceptions.ImportFailedException;

/**
 * @author Jamnic
 * ParserImpl class provides Moduler with static references to importer and exporter.
 */
public final class ParserImpl implements IParser {
	// Importowanie grafu - najpierw parsuj� pliki przez bibliotek� graphViz, potem wrapuj�
	// Jej obiekty do naszego formatu przy pomocy klasy Converter
	public Graph importGraph(File file) throws ImportFailedException {
		List<com.alexmerz.graphviz.objects.Graph> graphVizList = null;
		com.alexmerz.graphviz.Parser graphVizParser = new com.alexmerz.graphviz.Parser();
		
		try {
			graphVizParser.parse(new FileReader(file));
			graphVizList = graphVizParser.getGraphs();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ImportFailedException("File not found - path to the file may be invalid.");
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ImportFailedException("Parse exception - file content is invalid.");
		}

		Graph returnGraph = Importer.convert(graphVizList);
		if ( returnGraph == null ) throw new ImportFailedException("Parse exception - no graph found in file.");
		return returnGraph;
	}

	@Override
	public void exportGraph(Graph graph) throws ExportFailedException {
		exportGraph(graph, new File("DGV-" + new Date().getTime() + ".viz"));
	}

	@Override
	public void exportGraph(Graph graph, File file) throws ExportFailedException {
		try {
			Exporter.export(graph, file);
		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
			throw new ExportFailedException("File not found - path to the file may be invalid.");
		}
	}
	
}