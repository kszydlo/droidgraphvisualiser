package com.droidgraphvisualiser.parser.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.droidgraphvisualiser.modeler.exceptions.NoSuchVertexException;
import com.droidgraphvisualiser.modeler.structures.Edge;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Vertex;

/**
 * @author Jamnic
 * Auxiliary class, providing converting methods from graphViz formats to DGV format.
 */
final class Importer {
	
	/**
	 * Converts Alex Merz List of Graphs from Parser into big, composed from many Graphs DGV Graph.
	 * @param graphVizList
	 * @return Set of Graphs in new format.
	 */
	static Graph convert(List<com.alexmerz.graphviz.objects.Graph> graphVizList) {
		// Mo�na tu do�o�y� ��czenie kolejnych graf�w w jeden wielki
		com.alexmerz.graphviz.objects.Graph oldGraph = graphVizList.get(0);
		if ( oldGraph != null ) return Importer.convert(oldGraph);
		return null;
	}
	
	/**
	 * Converts Alex Merz Graph into DGV Graph.
	 * @param oldGraph
	 * @return Graph in new format.
	 */
	private static Graph convert(com.alexmerz.graphviz.objects.Graph oldGraph) {
		boolean isDirected = oldGraph.getType() == 2 ? true : false;
		Graph newGraph = new Graph(oldGraph.getId().getId(), isDirected );
		
		// Gromadzenie wszystkich kraw�dzi (z subgraf�w te�)
		List<com.alexmerz.graphviz.objects.Edge> oldEdges = new ArrayList<com.alexmerz.graphviz.objects.Edge>();
		oldEdges.addAll(oldGraph.getEdges());
		for ( com.alexmerz.graphviz.objects.Graph subgraph : oldGraph.getSubgraphs() )
			oldEdges.addAll(subgraph.getEdges());
		
		// Mapowania starych na nowe
		Map<com.alexmerz.graphviz.objects.Node, Vertex> vertexMap = new HashMap<com.alexmerz.graphviz.objects.Node, Vertex>();

		// Logika algorytmu
		for ( com.alexmerz.graphviz.objects.Edge oldEdge : oldEdges ) {
			com.alexmerz.graphviz.objects.Node firstNode = oldEdge.getSource().getNode();
			com.alexmerz.graphviz.objects.Node secondNode = oldEdge.getTarget().getNode();
			
			Vertex firstVertex = vertexMap.get(firstNode);
			if ( firstVertex == null ) {
				vertexMap.put(firstNode, firstVertex = convert(firstNode));
				newGraph.addVertex(firstVertex);
			}
			
			Vertex secondVertex = vertexMap.get(secondNode);
			if ( secondVertex == null ) {
				vertexMap.put(secondNode, secondVertex = convert(secondNode));
				newGraph.addVertex(secondVertex);
			}
			
			try {
				newGraph.addEdge(firstVertex, secondVertex, convert(oldEdge));
			} catch (NoSuchVertexException e) {
				e.printStackTrace();
			}
		}
		
		return newGraph;
	}
	
	/**
	 * Converts Alex Merz Vertex into DGV Vertex.
	 * @param oldVertex
	 * @return
	 */
	private static Vertex convert(com.alexmerz.graphviz.objects.Node oldVertex) {
		Vertex newVertex = new Vertex(oldVertex.getId().getId());
		for ( String key : oldVertex.getAttributes().keySet() )
			newVertex.setAttribute(key, oldVertex.getAttribute(key));
		return newVertex;
	}
	
	/**
	 * Converts Alex Merz Edge into DGV Edge.
	 * @param oldEdge
	 * @param firstVertex
	 * @param secondVertex
	 * @param isDirected
	 * @return Returns Edge in new format.
	 */
	private static Edge convert(com.alexmerz.graphviz.objects.Edge oldEdge) {
		Edge newEdge = new Edge();
		for ( String key : oldEdge.getAttributes().keySet() )
			newEdge.setAttribute(key, oldEdge.getAttribute(key));
		return newEdge;
	}
}
