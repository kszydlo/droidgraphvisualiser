package com.droidgraphvisualiser.parser;

import java.io.File;

import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.parser.exceptions.ExportFailedException;
import com.droidgraphvisualiser.parser.exceptions.ImportFailedException;

/**
 * Interface of Parser implementation, providing import and export of Graphs functions.
 * @author Jamnic
 */
public interface IParser {
	/**
	 * Imports set of Graph structures from file with DOT format.
	 * @param file
	 * @return Returns set of imported graphs (may be empty if no graph was found in file).
	 * @throws ImportFailedException
	 */
	Graph importGraph(File file) throws ImportFailedException;

	/**
	 * Export set of Graphs structures to the specified file, according to the DOT format.
	 * @param graph
	 * @param file
	 * @throws ExportFailedException
	 */
	void exportGraph(Graph graph, File file) throws ExportFailedException;
	
	/**
	 * Exports set of Graphs to the generated file, according to the DOT format. 
	 * @param graph
	 * @throws ExportFailedException
	 */
	void exportGraph(Graph graph) throws ExportFailedException;
}
