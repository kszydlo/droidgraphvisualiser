package com.droidgraphvisualiser.parser.test;

import java.io.File;

import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.parser.Parser;
import com.droidgraphvisualiser.parser.exceptions.ExportFailedException;
import com.droidgraphvisualiser.parser.exceptions.ImportFailedException;

public class ParserImplTest {
	/* Test */
	public static void run() {
		try {
			Graph graph = Parser.getInstance().importGraph(new File("testfiles//cluster.viz"));
			System.out.println(graph);
			Parser.getInstance().exportGraph(graph, new File("test.viz"));
			Graph graph2 = Parser.getInstance().importGraph(new File("test.viz"));
			System.out.println(graph2);
			Parser.getInstance().exportGraph(graph2);
		} catch (ImportFailedException e) {
			e.printStackTrace();
		} catch (ExportFailedException e) {
			e.printStackTrace();
		}
	}
}
