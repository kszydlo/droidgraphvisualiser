package com.droidgraphvisualiser.parser.exception;

public class ExportFailedException extends Exception {
	/* Fields */
	private static final long serialVersionUID = -1548772186935662383L;
	public String reason;
	
	/* Constructor */
	public ExportFailedException(String reason) {
		this.reason = reason;
	}
	
	/* Methods */
	@Override
	public void printStackTrace() {
		System.out.println(reason);
		super.printStackTrace();
	}
	
}
