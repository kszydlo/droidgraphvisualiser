package com.droidgraphvisualiser.parser.exceptions;

/**
 * @author Jamnic
 * Is thrown, when Parser finds errors during importing graphs from file.
 */
public class ImportFailedException extends Exception {
	/* Fields */
	private static final long serialVersionUID = -6633367997605101135L;
	public String reason;
	
	/* Constructor */
	public ImportFailedException(String reason) {
		this.reason = reason;
	}
	
	/* Methods */
	@Override
	public void printStackTrace() {
		System.out.println(reason);
		super.printStackTrace();
	}
	
}
