package com.droidgraphvisualiser.modeler.structures;

import java.util.HashMap;
import java.util.Map;

public class Vertex {

	/* Fields */
	private String label;
	private double posX;
	private double posY;
	private boolean showable;
	private int id = INDEX++;
	private static int INDEX = 0;
	private Map<String, Object> attributes = new HashMap<String, Object>();
	
	/* Constructors */
	public Vertex() { this(0.0, 0.0); }
	public Vertex(double _x, double _y) { this(_x,_y,""); }
	public Vertex(String _label) { this(0.0, 0.0, _label); }

	public Vertex(double _x, double _y, String _label) {
		this.posX = _x;
		this.posY = _y;
		this.label = _label;
	}

	/* SetGet */
	public void setLabel(String label) { this.label = label; }
	public void setXPosition(double _x) { this.posX = _x; }
	public void setYPosition(double _y) { this.posY = _y; }
	public boolean setAttribute(String key, Object value) {
		return attributes.put(key, value) == null ? true : false;
	}
	
	public boolean isShowable(){
		return showable;
	}
	
	public void setShowable(boolean showable){
		this.showable = showable;
	}

	public String getLabel() { return label; }
	public double getXPosition() { return posX; }
	public double getYPosition() { return posY; }
	public int getId() { return id; }
	public Map<String, Object> getAttributes() { return attributes; }
	public Object getAttribute(String key) { return attributes.get(key); }
	
	/* Methods */
	@Override
	public boolean equals(Object _object) {
		if ( _object instanceof Vertex && id == ((Vertex) _object).id ) return true;
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder("Vertex(" + id + ") '" + label + "' " + " attributes:\n");
		for ( String key : attributes.keySet() )
			string.append("\t" + key + " - " + attributes.get(key) + "\n"); 
		return string.toString();
	}
	
}