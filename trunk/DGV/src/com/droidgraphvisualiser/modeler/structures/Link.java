package com.droidgraphvisualiser.modeler.structures;

import java.util.HashSet;
import java.util.Set;

public class Link {
	
	/* Fields */
	private Vertex endpoint;
	private Vertex startpoint;
	private boolean showable;
	private int id = INDEX++;
	private static int INDEX = 0;
	private Tuple<Vertex, Vertex> tuple;
	private Set<Edge> edges = new HashSet<Edge>();
	static private Set<Tuple<Vertex, Vertex>> tuples = new HashSet<Tuple<Vertex, Vertex>>();

	/* Constructors */
	public Link(Vertex startpoint, Vertex endpoint) {
		this.endpoint = endpoint;
		this.startpoint = startpoint;
		tuples.add(tuple = new Tuple<Vertex, Vertex>(startpoint, endpoint, this));
		showable = true;
	}
	
	/* SetGet */	
	public Vertex getEndpoint() { return endpoint; }
	public Vertex getStartpoint() { return startpoint; }
	public int getId() { return id; }
	public Set<Edge> getEdges() { return edges; }
	
	public boolean isShowable(){
		return tuple.getFirst().isShowable() && tuple.getSecond().isShowable() && showable;
	}
	
	public void setShowable(boolean showable){
		this.showable = showable;
	}
	
	/**
	 * Returns Link object connecting two given Vertexes, or null if there isn't any.
	 * @param first
	 * @param second
	 * @return Link
	 */
	public static Link getLink(Vertex first, Vertex second) {
		for ( Tuple<Vertex, Vertex> tuple : tuples )
			if ( tuple.getFirst().equals(first) && tuple.getSecond().equals(second) )
				return tuple.getLink();
		return null;
	}
	
	/* Methods */
	public void addEdge(Edge edge) { edges.add(edge); }
	public void removeEdge(Edge edge) { edges.remove(edge); }

	/**
	 * Usuwaj�c Link u�ywajcie te� tej metody!
	 */
	public Link removeLink() {
		tuples.remove(tuple);
		return this;
	}
	
	public boolean isConnectedWith(Vertex _vertex) {
		return startpoint.equals(_vertex) || endpoint.equals(_vertex);
	}

	@Override
	public boolean equals(Object _object) {
		if ( _object instanceof Link && id == ((Link) _object).id ) return true;
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder("Link(" + id + ") connects:\n");
		stringBuilder.append("\t" + startpoint.getLabel() + "->" + endpoint.getLabel() + "\nEdges:\n");
		for ( Edge edge : edges )
			stringBuilder.append(edge + "\n");
		return stringBuilder.toString();
	}
	
	/* Inner Class */
	private class Tuple<U, V> {
		private U first;
		private V second;
		private Link link;

		Tuple(U first, V second, Link link) {
			this.first = first;
			this.second = second;
			this.link = link;
		}
		
		U getFirst() { return first; }
		V getSecond() { return second; }
		Link getLink() { return link; }
	}
	
}