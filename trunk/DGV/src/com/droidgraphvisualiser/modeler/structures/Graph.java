package com.droidgraphvisualiser.modeler.structures;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import android.util.Log;

import com.droidgraphvisualiser.modeler.exceptions.NoSuchVertexException;
import com.droidgraphvisualiser.modeler.filters.IFilter;
import com.droidgraphvisualiser.modeler.layout.ILayout;

public class Graph {

	/* Fields */
	private String label;
	private boolean isDirected = false;
	private Set<Link> links = new HashSet<Link>();
	private Set<Vertex> vertexes = new HashSet<Vertex>();
	
	/* Constructor */
	public Graph(String _label, boolean _isDirected) {
		this.label = _label;
		this.isDirected = _isDirected;
	}
	
	/* SetGet */
	public void setLabel(String label) { this.label = label; }

	public boolean isDirected() { return isDirected; }
	public String getLabel() { return label; }
	
	public Set<Vertex> getVertexes() {
		// to protect from modifications outside graph class
		return new HashSet<Vertex>(vertexes);
	}

	public Set<Link> getLinks() {
		// to protect from modifications outside graph class
		return new HashSet<Link>(links);
	}
	
	/* Methods */
	public void addVertex(Vertex vertex) {
		vertexes.add(vertex);
	}
	
	/**
	 * Removes Vertex from graph, and all it's Liks objects (containing Edges objects).
	 * @param vertex
	 * @throws NoSuchVertexException
	 */
	public void removeVertex(Vertex vertex) throws NoSuchVertexException {
		if ( vertexes.remove(vertex) ) {
			for ( Link link : links )
				if ( link.isConnectedWith(vertex) ) links.remove(link.removeLink());
		} else throw new NoSuchVertexException(); 
	}

	/**
	 * Creates a new Edge and adds it to the Link (if only it exists) or creates a new Link 
	 * between given Vertexes, assigns given Edge to this link.
	 * @param startpoint
	 * @param endpoint
	 * @throws NoSuchVertexException
	 */
	public void addEdge(Vertex startpoint, Vertex endpoint) 
			throws NoSuchVertexException {
		addEdge(startpoint, endpoint, new Edge());
	}

	/**
	 * Adds Edge to the Link (if only it exists) or creates a new Link between given Vertexes,
	 * assigns given Edge to this link.
	 * @param startpoint
	 * @param endpoint
	 * @param edge
	 * @throws NoSuchVertexException
	 */
	public void addEdge(Vertex startpoint, Vertex endpoint, Edge edge) 
			throws NoSuchVertexException {
		if ( !vertexes.contains(startpoint) || !vertexes.contains(endpoint) )
			throw new NoSuchVertexException();
		
		Link link = Link.getLink(startpoint, endpoint);
		if ( link == null ) links.add(link = new Link(startpoint, endpoint));
		edge.setLink(link);
		link.addEdge(edge);
	}
	
	/**
	 * Removes edge from graph. If link had its only edge, is also removed.
	 * @param edge
	 */
	public void removeEdge(Edge edge) {
		Link link = edge.getLink();
		link.removeEdge(edge);
		if ( link.getEdges().isEmpty() ) links.remove(link);
	}
	
	
	public void applyLayout(ILayout layout) {
		layout.apply(this);
	}
	
	/**
	 * Returns first occurence of Vertex with given Label
	 * @param label
	 * @return Vertex object if there is any Vertex with given label, or null if not.
	 */
	public Vertex findVertex(String label) {
		for ( Vertex vertex : vertexes )
			if ( vertex.getLabel().equals(label) ) return vertex;
		return null;
	}
	
	/**
	 * Finds set of Vertexes with specified label
	 * @param label
	 * @return Set containing all found Vertexes (may be empty).
	 */
	public Set<Vertex> findVertexAll(String label) {
		Set<Vertex> newSet = new HashSet<Vertex>();
		for ( Vertex vertex : vertexes )
			if ( vertex.getLabel().equals(label) ) newSet.add(vertex);
		return newSet;
	}
	
	public Map<Vertex, Set<Vertex>> getAdjacencyMap() {
		Map<Vertex, Set<Vertex>> result = new HashMap<Vertex, Set<Vertex>>();
		for (Vertex curr : vertexes){
			result.put(curr, new HashSet<Vertex>());
			for (Vertex v : vertexes){
				if (!curr.equals(v)){
					Link tmp = Link.getLink(curr, v);
					if (tmp != null && (tmp.getEdges().size() > 0)){
						result.get(curr).add(v);
					}
					if (!isDirected()){
						tmp = Link.getLink(v, curr);
						if (tmp != null && (tmp.getEdges().size() > 0)){
							result.get(curr).add(v);
						}
					}
				}
			}
		}
		return result;
	}
	
	public void resetFilters(){
		for (Vertex v : vertexes){
			v.setShowable(true);
		}
		for (Link l : links){
			for (Edge e : l.getEdges()){
				e.setShowable(true);
			}
			l.setShowable(true);
		}
	}
	
	public void applyFilter(IFilter filter){
		if (filter == null){
			resetFilters();
		} else {
			filter.apply(this);
		}
	}
	
	public void normalize(){
		if (vertexes.size() == 0){
			return;
		}
		
		double minX, minY, maxX, maxY;
		
		Vertex tmp = vertexes.iterator().next();
		
		minX = tmp.getXPosition();
		minY = tmp.getYPosition();
		maxX = tmp.getXPosition();
		maxY = tmp.getYPosition();
		
		for (Vertex v : vertexes){
			if (v.getXPosition() < minX){
				minX = v.getXPosition();
			} else if (v.getXPosition() > maxX){
				maxX = v.getXPosition();
			}
			
			if (v.getYPosition() < minY){
				minY = v.getYPosition();
			} else if (v.getYPosition() > maxY){
				maxY = v.getYPosition();
			}
		}
		
		double shiftX = 0.5 - (maxX + minX)/2;
		double shiftY = 0.5 - (maxY + minY)/2;		
		
		double scale = Math.abs(maxX + shiftX - 0.5);
		if (Math.abs(maxY + shiftY - 0.5) > scale){
			scale = Math.abs(maxY + shiftY - 0.5);
		}
		if (Math.abs(0.5 - minX + shiftX) > scale){
			scale = Math.abs(0.5 - minX + shiftX);
		}
		if (Math.abs(0.5 - minY + shiftY) > scale){
			scale = Math.abs(0.5 - minY + shiftY);
		}
		if (scale == 0.0){
			scale = 1.0;
		} else {
			scale = 0.45/scale;
		}
		
		Log.i("Normalization", "Scale: " + scale + " :: shiftX: " + shiftX + " :: shitftY: " + shiftY );
		
		for (Vertex v : vertexes){
			double x = v.getXPosition();
			double y = v.getYPosition();
			
			v.setXPosition( 0.5 + (((x + shiftX) - 0.5) * scale) );
			v.setYPosition( 0.5 + (((y + shiftY) - 0.5) * scale) );
			
			Log.i("Normalization" , v.getLabel() + "(" + v.getXPosition() + " , " + v.getYPosition() +")");
		}
		
	}
	
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder("Graph '" + label + "'\nVertexes: \n");
		for ( Vertex vertex : vertexes )
			string.append("\t" + vertex + "\n");
		
		string.append("Links: \n");
		for ( Link link : links )
			string.append("\t" + link + "\n");
		
		return string.toString();
	}
	
}