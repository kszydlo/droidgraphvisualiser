package com.droidgraphvisualiser.modeler.structures;

import java.util.HashMap;
import java.util.Map;

public class Edge {
	/* Fields */
	private Link link;
	private boolean showable;
	private int id = INDEX++;
	private static int INDEX = 0;
	private Map<String, Object> attributes = new HashMap<String, Object>();

	/* Constructors */
	public Edge(Link link) { this.link = link; }
	public Edge() { }
	
	/* SetGet */
	public void setLink(Link link) { this.link = link; }
	public boolean setAttribute(String key, Object value) {
		return attributes.put(key, value) == null ? true : false;
	}
	
	public boolean isShowable(){
		return link.isShowable() && showable;
	}
	
	public void setShowable(boolean showable){
		this.showable = showable;
	}

	public Link getLink() { return link; }
	public int getId() { return id; }
	public Map<String, Object> getAttributes() { return attributes; }
	public Object getAttribute(String key) { return attributes.get(key); }
	
	/* Methods */
	@Override
	public boolean equals(Object object) {
		if ( object instanceof Edge && id == ((Edge) object).id ) return true;
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder("Edge(" + id + ") attributes:\n");
		for ( String key : attributes.keySet() )
			stringBuilder.append("\t" + key + " - " + attributes.get(key) + "\n");
		return stringBuilder.toString();
	}
	
}