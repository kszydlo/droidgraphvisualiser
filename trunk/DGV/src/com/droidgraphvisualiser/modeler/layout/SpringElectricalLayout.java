package com.droidgraphvisualiser.modeler.layout;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Vertex;

public class SpringElectricalLayout implements ILayout {
	private int iterations;
	private final double A,B,C,D;
	
	public SpringElectricalLayout(int _iterations){
		this.iterations = _iterations;
		this.A = 1.0;
		this.B = 1.0;
		this.C = 0.1;
		this.D = 0.01;
		
	}
	
	public SpringElectricalLayout(){
		this(100);
	}
	
	@Override
	public void apply(Graph _graph) {
		Random rand = new Random();
		Map<Vertex, Set<Force>> forces = new HashMap<Vertex, Set<Force>>();
		Map<Vertex, Set<Vertex>> adjacentVertexes = _graph.getAdjacencyMap();
		for (Vertex v : _graph.getVertexes()){
			v.setXPosition(rand.nextDouble());
			v.setYPosition(rand.nextDouble());
			forces.put(v, new HashSet<Force>());
		}
		
		Set<Vertex> vertexes = _graph.getVertexes();
	
		for (int i = 0; i< iterations; i++){
			for (Vertex current : vertexes){
				forces.get(current).clear();
				for (Vertex v : vertexes){
					if (!v.equals(current)){
						double distX = current.getXPosition() - v.getXPosition();
						double distY = current.getYPosition() - v.getYPosition();
						if (adjacentVertexes.get(current).contains(v)){
							Force f = new Force( 
									Math.signum(distX) * A * 
									Math.log10( Math.abs(distX/B )),
									Math.signum(distY) * A * 
									Math.log10( Math.abs(distY/B )));
							forces.get(current).add(f);
						} else {
							Force f = new Force(
									Math.signum(-distX) *
									C / Math.pow(distX*100, 2),
									Math.signum(- distY) *
									C / Math.pow(distY*100, 2));
							forces.get(current).add(f);
						}
					}
				}
			}
			for (Vertex v : vertexes){
				for (Force f : forces.get(v)){
					f.apply(v);
				}
			}
			
			for (Vertex v : vertexes){
//				Log.i("Position norm", "Iteration " + i + ": " + v.getLabel() + "(" + v.getXPosition() + " , " + v.getYPosition() +")");
			}
		}
		
		_graph.normalize();
	}

	
	public class Force{
		double vectorX;
		double vectorY;
		
		public Force(double _vectorX, double _vectorY){
			this.vectorX = _vectorX;
			this.vectorY = _vectorY;
		}
		
		public void apply(Vertex _vertex){
//			Log.i("Force", _vertex.getLabel() + "(" + _vertex.getXPosition() + " . " 
//					+ _vertex.getYPosition() +")  ->  (" + vectorX + ", " + vectorY + ")");
			_vertex.setXPosition(D * vectorX + _vertex.getXPosition());
			_vertex.setYPosition(D * vectorY + _vertex.getYPosition());
			
		}
	}
}
