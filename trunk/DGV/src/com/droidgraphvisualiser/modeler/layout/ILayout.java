package com.droidgraphvisualiser.modeler.layout;

import com.droidgraphvisualiser.modeler.structures.Graph;

public interface ILayout {
	public void apply(Graph _graph);
}
