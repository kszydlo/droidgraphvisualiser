package com.droidgraphvisualiser.modeler.layout;

import java.util.Set;

import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Vertex;

public class CircularLayout implements ILayout {
	private double centerX;
	private double centerY;
	private double radius;
	
	public CircularLayout(){
		centerX = 0.5;
		centerY = 0.5;
		radius = 0.45;
	}
	
	public void setCenter(double _x, double _y){
		if ((radius + _x > 1) || (radius + _y > 1) || (radius - _y < 0) || (radius - _x < 0)){
			throw new UnsupportedOperationException("Incorect coordinates for center with current radius");
		} else {
			centerX = _x;
			centerY = _y;
		}
	}
	
	public void setRadius(double _r){
		if ((_r + centerX > 1) || (_r + centerY > 1) || (_r - centerY < 0) || (_r - centerX < 0)){
			throw new UnsupportedOperationException("Incorect value of radius for current center of circle");
		} else {
			radius = _r;
		} 
	}

	@Override
	public void apply(Graph _graph) {
		Set<Vertex> vertexes = _graph.getVertexes();
		if (!vertexes.isEmpty()){
			if (vertexes.size() == 1){
				for (Vertex v : vertexes){
					v.setXPosition(centerX);
					v.setYPosition(centerY);
				}
			} else {
				double degreeStep = 360.0/((double) vertexes.size());
				double degree = 0.0;
				for (Vertex v : vertexes){
					v.setXPosition(centerX + radius * Math.sin(Math.toRadians(degree)));
					v.setYPosition(centerY + radius * Math.cos(Math.toRadians(degree)));
					degree += degreeStep;
				}
			}
		}
	}

}
