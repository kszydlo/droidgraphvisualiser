package com.droidgraphvisualiser.modeler.exceptions;

/**
 * @author Jamnic
 * Is thrown, when an Vertex object is getting removed, but it is not linked with Graph.
 */
public class NoSuchVertexException extends Exception {
	/* Fields */
	private static final long serialVersionUID = -1548772186935662383L;
}
