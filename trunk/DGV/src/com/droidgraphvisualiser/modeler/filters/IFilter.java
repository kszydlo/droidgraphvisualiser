package com.droidgraphvisualiser.modeler.filters;

import com.droidgraphvisualiser.modeler.structures.Graph;

public interface IFilter {
	
	public void apply(Graph graph);
}
