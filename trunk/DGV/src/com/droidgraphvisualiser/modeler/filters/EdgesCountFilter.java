package com.droidgraphvisualiser.modeler.filters;

import java.util.HashMap;
import java.util.Map;

import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Link;
import com.droidgraphvisualiser.modeler.structures.Vertex;

public class EdgesCountFilter implements IFilter {
	private int limit;
	private Dependency dependency;
	
	public EdgesCountFilter(int limit, Dependency dependency){
		this.limit = limit;
		this.dependency = dependency;
	}
	
	public EdgesCountFilter(){
		this(5, Dependency.LESSER);
	}
	
	@Override
	public void apply(Graph graph) {
		Map<Vertex, Integer> edgesCount = new HashMap<Vertex, Integer>();
		for (Vertex v : graph.getVertexes()){
			edgesCount.put(v, 0);
		}
		for (Link l : graph.getLinks()){
			edgesCount.put(l.getStartpoint(), edgesCount.get(l.getStartpoint()) + l.getEdges().size());
			if (l.getStartpoint() != l.getEndpoint()){
				edgesCount.put(l.getEndpoint(), edgesCount.get(l.getEndpoint()) + l.getEdges().size());
			}
		}
		
		for (Vertex v : edgesCount.keySet()){
			switch (edgesCount.get(v).compareTo(limit)){
				case 1: {
						if (!dependency.equals(Dependency.GREATER) && !dependency.equals(Dependency.GREATEREQUAL)){
							v.setShowable(false);
						}
					break;
					}
				case 0: {
					if (!dependency.equals(Dependency.EQUAL) && !dependency.equals(Dependency.GREATEREQUAL) && !dependency.equals(Dependency.LESSEREQUAL)){
						v.setShowable(false);
					}
					break;
				}
				case -1: {
					if (!dependency.equals(Dependency.LESSER) && !dependency.equals(Dependency.LESSEREQUAL)){
						v.setShowable(false);
					}
				}
			}
		}

	}
	
	public enum Dependency{
		GREATER,
		GREATEREQUAL,
		EQUAL,
		LESSEREQUAL,
		LESSER;
	}

}
