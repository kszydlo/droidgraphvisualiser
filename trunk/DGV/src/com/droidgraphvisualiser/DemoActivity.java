package com.droidgraphvisualiser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author arch
 *
 * Activity created to present library capabilities usage and how to use it.
 * Makes user able either to open *.viz file from host filesystem or select random test file from builtin assets.
 */
public class DemoActivity extends Activity {

	/**
	 * Name of asset directory which stores sample graph files.
	 */
	final static String TESTFILES_DIR = "testfiles";
	
	/**
	 * Name of temporary cache file used when loading graph from assets. 
	 */
	final static String CACHE_FILE = "testgraph.viz";
	
	/**
	 * Stored full filepath to loaded file containing graph.
	 */
	String filepath = null;

	/**
	 * Basic activity constructor.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_demo);
	}

	/**
	 * Onclick event handler for button.
	 * Reads text input and treats it as filepath.
	 * @param view
	 */
	public void loadFileAction(View view) {
		EditText field = (EditText) findViewById(R.id.editText1);
		filepath = field.getText().toString();
		displayAction(view);
	}

	/**
	 * Onclick event handler for button.
	 * Loads a random example graph (using a cache file) from provided assets.
	 * @param view
	 * @throws IOException when cannot create cache file
	 */
	public void randomDisplayAction(View view) throws IOException {
		try {
			filepath = loadRandomFile();
		} catch (IOException e) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Error when creating temporary file.", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			return;
		}
		displayAction(view);
	}

	/**
	 * Loads the file and, if successful, starts new intent with DGVActivity.
	 * Forwards the filepath using 'filepath' intent extra.
	 * @param view
	 */
	private void displayAction(View view) {
		File file = new File(filepath);
		if (file.exists()) {
			Intent i = new Intent(this, DGVActivity.class);
			i.putExtra("filepath", filepath);
			startActivity(i);
		} else {
			Toast toast = Toast.makeText(getApplicationContext(),
					"File not found:\n" + filepath, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
	}

	/**
	 * Selects random file from asset test files and creates a temporary file in the host filesystem.
	 * The file needs to be created as the parsing library accepts only File objects, not InputStream.
	 * 
	 * @return String absolute path for created cache file
	 * @throws IOException when cannot create cache file
	 */
	private String loadRandomFile() throws IOException {
		deleteCache(getApplicationContext());
		String testfiles[] = getAssets().list(TESTFILES_DIR);

		InputStream ins = getAssets().open(TESTFILES_DIR + File.separator +
				testfiles[new Random().nextInt(testfiles.length - 1)]);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int size = 0;
		byte[] buffer = new byte[1024];
		while ((size = ins.read(buffer, 0, 1024)) >= 0) {
			outputStream.write(buffer, 0, size);
		}
		ins.close();
		buffer = outputStream.toByteArray();

		File file = new File(getApplicationContext().getCacheDir(), CACHE_FILE);
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(buffer);
		fos.close();
		
		return file.getAbsolutePath();
	}

	/**
	 * Clears cache directory for this application.
	 * @param context
	 */
	private void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			if (dir != null && dir.isDirectory()) {
				deleteDir(dir);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Recursive directory removal.
	 * @param dir directory to remove
	 * @return boolean is successful?
	 */
	private boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}
}
