package com.droidgraphvisualiser.test;

import java.io.File;
import java.util.Random;

import com.droidgraphvisualiser.modeler.exceptions.NoSuchVertexException;
import com.droidgraphvisualiser.modeler.layout.CircularLayout;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Vertex;
import com.droidgraphvisualiser.parser.Parser;
import com.droidgraphvisualiser.parser.exceptions.ImportFailedException;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Environment;
import android.util.AttributeSet;

/**
 * @author arch
 * 
 * Test wrapper-class for CanvasBlock object used to display demo graphs.
 * @deprecated No longer needed - current implementation delivers built-in test assets.
 * @see com.droidgraphvisualiser.DemoActivity
 */
public class CanvasBlockTest extends com.droidgraphvisualiser.presenter.CanvasBlock {

	/**
	 * @deprecated
	 * Constructor calling test method after initialization. 
	 * 
	 * @param context
	 */
	public CanvasBlockTest(Context context) {
		super(context);
		//runTest(false);
		runRandomTest();
	}

	/**
	 * @deprecated
	 * Constructor calling test method after initialization.
	 * 
	 * @param context
	 * @param attrs
	 */
	public CanvasBlockTest(Context context, AttributeSet attrs) {
		super(context, attrs);
		//runTest(false);
		runRandomTest();
	}

	/**
	 * @deprecated
	 * Constructor calling test method after initialization.
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public CanvasBlockTest(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//runTest(false);
		runRandomTest();
	}
	
	/**
	 * TROLOLOLOLO
	 * @param which test?
	 * @deprecated
	 */
	public void runTest(boolean which) {
		if (which) {
			runFirstTest();
		} else {
			runSecondTest();
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}
	
	/**
	 * Creates a hardcoded graph and displays it.
	 */
	private void runFirstTest() {
		Graph g = new Graph("ala123", false);
		Vertex v1 = new Vertex("aeiouy");
		Vertex v2 = new Vertex("wierzch");
		Vertex v3 = new Vertex("o�ek");
		Vertex v4 = new Vertex("kora");
		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v4);
		try {
			g.addEdge(v1, v2);
			g.addEdge(v1, v3);
			g.addEdge(v1, v3);
			g.addEdge(v1, v3);
			g.addEdge(v2, v3);
			g.addEdge(v2, v4);
		} catch (NoSuchVertexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		g.applyLayout(new CircularLayout());
		setGraph(g);
	}
	
	/**
	 * Selects graph 'cluster' and calls test method.
	 */
	private void runSecondTest() {
		runSecondTest("cluster");
	}
	
	/**
	 * Displays graph from test graph directory.
	 * @param file filename from test file directory (without extension)
	 */
	private void runSecondTest(String file) {
		try {
			Graph g = Parser.getInstance().importGraph(
					new File(Environment.getExternalStorageDirectory().getPath()
							+ "/Download/testfiles/" + file + ".viz")
					);
			g.applyLayout(new CircularLayout());
			setGraph(g);
		} catch (ImportFailedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Selects a random test from hardcoded test file list and calls test method.
	 */
	private void runRandomTest() {
		String testfiles[] = {
				"cluster",
				"crazy",
				"datastruct",
				"ER",
				"fdpclust",
				"fsm",
				"hello",
				"inet",
				"NodeEdge",
				"process",
				"profile",
				"sdh",
				"softmain",
				"switch",
				"test",
				"transparency",
				"unix",
				"world"
		};
		runSecondTest(testfiles[new Random().nextInt(testfiles.length-1)]);
	}
   
}
