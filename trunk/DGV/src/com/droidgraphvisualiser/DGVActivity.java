/**
 * 
 */
package com.droidgraphvisualiser;

import java.util.ArrayList;
import java.util.Date;

import com.droidgraphvisualiser.modeler.layout.CircularLayout;
import com.droidgraphvisualiser.modeler.layout.SpringElectricalLayout;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Vertex;
import com.droidgraphvisualiser.presenter.CanvasBlock;
import com.droidgraphvisualiser.presenter.Constants;
import com.droidgraphvisualiser.presenter.Stylesheet;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;


/**
 * @author arch
 *
 * Activity responsible for rendering graph and navigation menus.
 * Also registers some onClick events with logic for graph management purposes.
 * 
 * MUST be started with an extra 'filename' String param with value consisting full filepath for the graph file. 
 */
public class DGVActivity extends FragmentActivity {
	
	/**
	 * List of currently displayed vertices (filtered).
	 */
	private ArrayList<DGVActivity.FilterableVertex> vertices;
	
	/**
	 * Full path of *.viz file to load.
	 * Has to be provided using intent extra 'filepath'.
	 */
	private String filepath;

	/**
	 * Acts as constructor.
	 * Reads the intent extra 'filename' to load graph file.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (vertices == null)
			vertices = new ArrayList<DGVActivity.FilterableVertex>();
		if (filepath == null) {
			Bundle extras = getIntent().getExtras();
			filepath = extras.getString("filepath");	// TODO mind it in the docs
		}
		setContentView(R.layout.activity_dgv);
    }
	
	/**
	 * Getter for loaded graph file path.
	 * @return full path with filename
	 */
	public String getFilepath() {
		return filepath;
	}
	
	
	/*********************************\
	|*********************************|
	|* ONCLICK EVENTS FOR MENU ITEMS *|
	|*********************************|
	\*********************************/

	
	/**************\
	|* LAYOUT TAB *|
	\**************/
	
	/**
	 * Onclick event handler for button.
	 * Switches graph's layout to Spring Electrical.
	 * @param view
	 */
	public void springElectricalLayout_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		graph.getGraph().applyLayout(new SpringElectricalLayout());
		graph.invalidate();
	}
	
	/**
	 * Onclick event handler for button.
	 * Switches graph's layout to Circular.
	 * @param view
	 */
	public void circularLayout_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		graph.getGraph().applyLayout(new CircularLayout());
		graph.invalidate();
	}
	
	/**
	 * Onclick event handler for button.
	 * Toggles display of vertex labels on the graph.
	 * @param view
	 */
	public void labelsCheckBox_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		CheckBox toggle = (CheckBox)view;
		graph.toggleLabels(toggle.isChecked());
	}
	
	/**
	 * Onclick event handler for button.
	 * Changes graph stylesheet into selected one.
	 * 
	 * Specifies stylesheets other than default. 
	 * @param view
	 */
	public void changeColorScheme_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		Stylesheet style = new Stylesheet();
		switch (view.getId()) {
			case R.id.defaultColorScheme:
				break;
			case R.id.custom1ColorScheme:
				style.setVertexColor(Color.BLUE);
				style.setLabelColor(Color.YELLOW);
				style.setLinkColor(Color.GREEN);
				break;
			case R.id.custom2ColorScheme:
				style.setVertexColor(Color.GRAY);
				style.setLabelColor(Color.GREEN);
				style.setTextColor(Color.RED);
				style.setLinkColor(Color.MAGENTA);
				break;
		}
		graph.setStylesheet(style);
	}
	
	/**************\
	|* FILTER TAB *|
	\**************/
	
	/**
	 * Onclick event handler for button.
	 * Filters selected vertices and links. Communicates with Modeler to toggle visibility. 
	 * @param view
	 */
	public void filterCheckBox_onClick(View view) {
		for (FilterableVertex element : vertices) {
			element.getVertex().setShowable(element.getCheckBox().isChecked());
		}
		findViewById(R.id.canvas_block).invalidate();
	}
	
	/**
	 * A subclass to create checkbox vertices in layout.
	 * Needed to actually filter vertices.
	 * 
	 * @TODO extend CheckBox instead
	 */
	private class FilterableVertex {
		private CheckBox checkbox;
		private Vertex vertex;
		
		/**
		 * Sets filter checkboxes for each vertex (with onClick listener).
		 * @param checkbox View item
		 * @param vertex Vertex object associated with the checkbox
		 */
		public FilterableVertex(CheckBox checkbox, Vertex vertex) {
			this.checkbox = checkbox;
			this.vertex = vertex;
			CharSequence text = "[" + vertex.getLabel() + "]\nx=" + vertex.getXPosition() + "\ny=" + vertex.getYPosition();
			checkbox.setText(text);
			checkbox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					filterCheckBox_onClick(v);
				}
			});
		}
		
		/**
		 * @return CheckBox
		 */
		public CheckBox getCheckBox() {
			return checkbox;
		}
		/**
		 * @return Vertex
		 */
		public Vertex getVertex() {
			return vertex;
		}
	}

	/**
	 * Onclick event handler for button.
	 * Prevents user from filtering vertices displayed on the graph. Sets all vertices as visible.
	 * @param view
	 */
	public void allVerticesRadio_onClick(View view) {
		((CanvasBlock)findViewById(R.id.canvas_block)).restoreGraph();	// restore original vertices
		Graph graph = ((CanvasBlock)findViewById(R.id.canvas_block)).getGraph();
		
		if (vertices.isEmpty()) {	// 1st run - TODO move to a better place
			LinearLayout checkboxes = (LinearLayout)findViewById(R.id.verticesCheckboxes);
			checkboxes.removeAllViews();
		
			vertices.clear();	// we delete all vertices from current list 
			for (Vertex vert : graph.getVertexes()) {
				FilterableVertex element = new FilterableVertex(new CheckBox(this), vert);
				checkboxes.addView(element.getCheckBox());
				vertices.add(element);
			}
		}
		for (FilterableVertex element : vertices) {
			element.getCheckBox().setChecked(true);		// ALL
			element.getCheckBox().setEnabled(false);	// we cannot change them on this layout
		}
		findViewById(R.id.canvas_block).invalidate();
	}
	
	/**
	 * Onclick event handler for button.
	 * Enables user to filter vertices displayed on the graph.
	 * @param view
	 */
	public void customVerticesRadio_onClick(View view) {		
		for (FilterableVertex element : vertices) {
			element.getCheckBox().setEnabled(true);	// reenable layout change
		}
	}
	
	/************\
	|* ZOOM TAB *|
	\************/
	
	/**
	 * Onclick event handler for button.
	 * Zooms in graph display size.
	 * @param view
	 */
	public void zoomInButton_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		graph.setScale(graph.getScale() + Constants.SCALE_STEP);
	}
	
	/**
	 * Onclick event handler for button.
	 * Zooms out graph display size.
	 * @param view
	 */
	public void zoomOutButton_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		graph.setScale(graph.getScale() - Constants.SCALE_STEP);
	}
	
	/**
	 * Onclick event handler for button.
	 * Resets graph display size to default
	 * @param view
	 */
	public void resetButton_onClick(View view) {
		CanvasBlock graph = (CanvasBlock)findViewById(R.id.canvas_block);
		graph.resetScale();
	}
	
	/************\
	|* SAVE TAB *|
	\************/
	
	/**
	 * Saves a view into file. After operation a Toast is shown to user.
	 * The file is stored in DCIM directory by default.
	 * @param view
	 */
	public void saveButton_onClick(View view) {
		View graph = findViewById(R.id.canvas_block);
		Bitmap image = Bitmap.createBitmap(graph.getWidth(), graph.getHeight(), Bitmap.Config.RGB_565);
		graph.draw(new Canvas(image));
		// FIXME filename -> graph name instead of date
		String filename = DateFormat.format("yyyy-MM-dd_kk-mm", new Date(System.currentTimeMillis())).toString();

		CharSequence text;
		try {
			String filepath = Images.Media.insertImage(getContentResolver(), image, filename, null);
			if (filepath == null) {
				throw new Exception();
			}
			// FIXME -> real save path
			text = getText(R.string.ToastSaveOK) + " " + filepath;
		} catch (Exception e) {
			text = getText(R.string.ToastSaveERROR);
		}
		Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	
}
