package com.droidgraphvisualiser.presenter;

import android.graphics.Color;

/**
 * @author arch
 * 
 * Class which holds graph stylesheets for Presenter module.
 * Values kept are int type available in class android.graphics.Color.
 * 
 * The class provides capability to specify colors on the screen for:
 * - vertices
 * - links between vertices
 * - text label over the vertex
 * - background behind the vertex label
 */
public class Stylesheet {
	
	private int vertexColor;
	private int linkColor;
	private int labelColor;
	private int textColor;
	
	/**
	 * Default constructor for setting default style values.
	 * 
	 * Standard style is:
	 * - black vertices
	 * - red links
	 * - black vertex label
	 * - cyan vertex label background
	 */
	public Stylesheet() {
		vertexColor = Color.BLACK;
		linkColor = Color.RED;
		labelColor = Color.CYAN;
		textColor = Color.BLACK;
	}
	
	/**
	 * Sets color of displayed vertices.
	 * @param color new color
	 */
	public void setVertexColor(int color) {
		vertexColor = color;
	}
	/**
	 * Sets color for links between vertices.
	 * @param color new color
	 */
	public void setLinkColor(int color) {
		linkColor = color;
	}
	/**
	 * Sets background color for the vertex description.
	 * @param color new color
	 */
	public void setLabelColor(int color) {
		labelColor = color;
	}
	/**
	 * Sets text color over the vertex (with vertex name).
	 * @param color new color
	 */
	public void setTextColor(int color) {
		textColor = color;
	}
	
	/**
	 * Getter for vertex color.
	 * @return Color
	 */
	public int getVertexColor() {
		return vertexColor;
	}
	/**
	 * Getter for link color.
	 * @return Color
	 */
	public int getLinkColor() {
		return linkColor;
	}
	/**
	 * Getter for label color.
	 * @return Color
	 */
	public int getLabelColor() {
		return labelColor;
	}
	/**
	 * Getter for text color.
	 * @return Color
	 */
	public int getTextColor() {
		return textColor;
	}
	
}
