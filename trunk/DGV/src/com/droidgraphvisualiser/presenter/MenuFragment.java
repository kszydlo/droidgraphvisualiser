/**
 * 
 */
package com.droidgraphvisualiser.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

import com.droidgraphvisualiser.DGVActivity;
import com.droidgraphvisualiser.R;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Link;

/**
 * @author Daniel
 *
 * Class responsible for displaying navigation menu inside view fragment.
 * See res/layout/menu_fragment.xml
 */
public class MenuFragment extends Fragment{

	private static final String TAB_LAYOUT = "Layout";
	private static final String TAB_FILTERS = "Filters";
	private static final String TAB_ZOOM = "Zoom";
    private static final String TAB_EDGES = "Edges";
    private static final String TAB_EDITOR = "Editor";
    private static final String TAB_SAVE = "Save";

    /**
     * Tab host fragment view (and menu_fragment as well).
     */
    private TabHost mTabHost;

	/**
	 * Default create method for this fragment.
	 * Inflates the fragment as non-root element.
	 */
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View mRoot = inflater.inflate(R.layout.menu_fragment, container, false);
		mTabHost = (TabHost) mRoot;
		return mRoot;
	}

    /**
     * Overrides default method after application startup.
     * Prepares all tabs for display purposes.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        setupTabs();        
        setupTabData();
    }
 
    /**
     * Calls library tab setup and adds all application specific clickable tabs.
     */
    private void setupTabs() {
        mTabHost.setup(); // you must call this before adding your tabs!
        TabWidget tabWidget = mTabHost.getTabWidget();
		tabWidget.setOrientation(LinearLayout.VERTICAL);

        mTabHost.addTab(newTab(TAB_LAYOUT, R.string.tab_layout, R.id.tab_1));
        mTabHost.addTab(newTab(TAB_FILTERS, R.string.tab_filters, R.id.tab_2));
        mTabHost.addTab(newTab(TAB_ZOOM, R.string.tab_zoom, R.id.tab_3));
        mTabHost.addTab(newTab(TAB_EDITOR, R.string.tab_editor, R.id.tab_4));
        mTabHost.addTab(newTab(TAB_EDGES, R.string.tab_edges, R.id.tab_5));
        mTabHost.addTab(newTab(TAB_SAVE, R.string.tab_save, R.id.tab_6));
    }
 
    /**
     * Generates a tab using provided parameters.
     * 
     * @param tag unique name for the tab
     * @param labelId string reference for label
     * @param tabContentId Layout reference for tab content 
     * @return TabSpec newly created tab
     */
    private TabSpec newTab(String tag, int labelId, int tabContentId) {
        Log.d("TabSpec", "buildTab(): tag=" + tag);
 
        View indicator = LayoutInflater.from(getActivity()).inflate(
                R.layout.tab_indicator, mTabHost.getTabWidget() , false);
               // (ViewGroup) mRoot.findViewById(android.R.id.tabs), false);
        
       // View tabIndicator = inflater.inflate(R.layout.tab_indicator , tabHost.getTabWidget(), false);
       // ((TextView) indicator.findViewById(R.id.text)).setText(labelId);
        final TextView tv = (TextView) indicator.findViewById(R.id.title);
	       tv.setText(tag);
        TabSpec tabSpec = mTabHost.newTabSpec(tag);

        tabSpec.setIndicator(indicator);
        tabSpec.setContent(tabContentId);

        return tabSpec;
    }
    
	/**
	 * Sets start display options for tabs.
	 * Use it to define start data for the tabs.
	 *  
	 * Currently this method does following actions:
	 * - [DISABLED] set not-yet-working layouts as unavailable for selection
	 * - emulate click to display "all vertices" in the graph
	 * - generate information about all edges inside tab 5 (Edges)
	 */
	private void setupTabData() {
		com.droidgraphvisualiser.DGVActivity activity = (DGVActivity) getActivity();
		
		// hides different layout types
		//((RadioButton)activity.findViewById(R.id.forceDrivenRadioButton)).setEnabled(false);
		
		// shows all vertices
		activity.allVerticesRadio_onClick(activity.findViewById(R.id.allVerticesRadio));
		
		// generate edges information to tab 5
		Graph graph = ((CanvasBlock)activity.findViewById(R.id.canvas_block)).getGraph();
		ScrollView edges = (ScrollView)activity.findViewById(R.id.edgesInformation);
		TextView child = new TextView(activity);
		for (Link link : graph.getLinks()) {
			child.append(link.toString() + "\n");
		}
		edges.addView(child);
		
	}
    
}
