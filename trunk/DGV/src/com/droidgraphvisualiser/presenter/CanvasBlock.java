/**
 * 
 */
package com.droidgraphvisualiser.presenter;

import java.io.File;

import com.droidgraphvisualiser.modeler.layout.CircularLayout;
import com.droidgraphvisualiser.modeler.structures.Graph;
import com.droidgraphvisualiser.modeler.structures.Link;
import com.droidgraphvisualiser.modeler.structures.Vertex;
import com.droidgraphvisualiser.parser.Parser;
import com.droidgraphvisualiser.parser.exceptions.ImportFailedException;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

/**
 * @author arch
 *
 * Base view rendered inside graph_fragment.
 * Object is responsible for rendering the graph itself and the way it is displayed - zoom, translation, stylesheets etc.
 */
public class CanvasBlock extends View {
	
	/**
	 * Current graph visible inside the block.
	 */
	private Graph graph;
	
	/**
	 * Current graph display stylesheet.
	 */
	private Stylesheet stylesheet;
	
	/**
	 * Canvas object.
	 */
	private Canvas canvas;
	
	/**
	 * Current scale of visible graph.
	 */
	private float scaleFactor = Constants.DEFAULT_SCALE;
	
	/**
	 * Pinch zoom gesture detector.
	 */
	private ScaleGestureDetector scaleGestureDetector;
	
	/**
	 * Currently dragged vertex. Set only during dragging operation.
	 */
	private Vertex draggedVertex;

	/**
	 * Current X-coordinate translation of visible graph 
	 */
	private float translationX = Constants.DEFAULT_TRANSLATION_X;
	
	/**
	 * Current Y-coordinate translation of visible graph 
	 */
	private float translationY = Constants.DEFAULT_TRANSLATION_Y;
	
	/**
	 * Are labels currently shown?
	 */
	private boolean showLabels = false;

	public CanvasBlock(Context context) {
		super(context);
		createCanvasBlock(context);
	}

	public CanvasBlock(Context context, AttributeSet attrs) {
		super(context, attrs);
		createCanvasBlock(context);
	}
	
	public CanvasBlock(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		createCanvasBlock(context);
	}
	
	/**
	 * Helper method to prevent constructor redundancy.	Sets default settings and installs a gesture detector.
	 * 
	 * It detects whether we use it standalone or with DGVActivity.
	 * If the latter, we render a graph.
	 * @param context
	 */
	private void createCanvasBlock(Context context) {
		stylesheet = new Stylesheet();
		if (context instanceof com.droidgraphvisualiser.DGVActivity) {
			try {
				Graph g = Parser.getInstance().importGraph(
						new File(((com.droidgraphvisualiser.DGVActivity)context).getFilepath())
					);
				g.applyLayout(new CircularLayout());	// by default
				setGraph(g);
			} catch (ImportFailedException e) {
				e.printStackTrace();	// FIXME hide debug
			}
		}
		setFocusable(true);
		scaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());		
	}
	
	/**
	 * Sets a template to display all items. Invalidates the view.
	 * @param style
	 */
	public void setStylesheet(Stylesheet style) {
		stylesheet = style;
		invalidate();
	}
	
	/**
	 * Setter for displayed graph.
	 * @param graph new graph
	 */
	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	/**
	 * Getter for currently displayed graph.
	 * @return Graph
	 */
	public Graph getGraph() {
		return graph;
	}
	
	/**
	 * Restores default visibility of graph vertices (show all).
	 */
	public void restoreGraph() {
		for (Vertex vert : graph.getVertexes()) {
			vert.setShowable(true);
		}
	}
	
	/**
	 * TODO graph change history
	 */
	
	/**
	 * Draws a graph instance.
	 * @param graph to import and display
	 */
	public void drawGraph(Graph graph) {
		setGraph(graph);
		for (Link link : graph.getLinks()) {
			if (link.isShowable())
				drawLink(link);
		}
		for (Vertex vert : graph.getVertexes()) {
			if (vert.isShowable())
				drawVertex(vert);
		}
		if (showLabels) {
			for (Vertex vert : graph.getVertexes()) {
				if (vert.isShowable())
					drawVertexLabel(vert);
			}
		}
	}
	
	/**
	 * Draws selected vertex in the Canvas with default style.
	 * @param vert vertex to draw
	 * 
	 * @TODO HARDCODED vertex radius 
	 */
	protected void drawVertex(Vertex vert) {
		int radius = Constants.VERTEX_RADIUS;
		
		Paint paint = new Paint();
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(stylesheet.getVertexColor());
		canvas.drawCircle(getX(vert.getXPosition()), getY(vert.getYPosition()), radius, paint);
	}
	
	/**
	 * Draws a label over selected vertex in the Canvas with currently selected style.
	 * The text is highlighted.
	 * @param vert
	 */
	protected void drawVertexLabel(Vertex vert) {
		Paint paint = new Paint();
		float x = getX(vert.getXPosition());
		float y = getY(vert.getYPosition());
		float offset = Constants.LABEL_OFFSET;
		float textSize = Constants.LABEL_SIZE;
		
		/**
		 * @TODO HARDCODED styles
		 */
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(stylesheet.getLabelColor());
		paint.setTextSize(textSize);
		paint.setTextAlign(Align.CENTER);
		FontMetrics fm = new FontMetrics();
		paint.getFontMetrics(fm);
		
		float textWidth = paint.measureText(vert.getLabel());
		
		Rect rect = new Rect();
		rect.set((int)Math.ceil(offset + x - textWidth/2), (int)Math.floor(-textSize + offset + y), (int)Math.floor(-offset + x + textWidth/2), (int)Math.ceil(offset + y));
		canvas.drawRect(rect, paint);
		paint.setColor(stylesheet.getTextColor());
		canvas.drawText(vert.getLabel(), x, 2*offset + y + -(fm.ascent + fm.descent) / 2, paint);
	}
	
	/**
	 * Draws links between vertices - as dense as many edges are between.
	 * @param link connection between two vertices
	 */
	protected void drawLink(Link link) {
		Vertex startpoint = link.getStartpoint();
		Vertex endpoint = link.getEndpoint();
		
		Paint paint = new Paint();
		paint.setColor(stylesheet.getLinkColor());
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setStrokeWidth(link.getEdges().size());
		canvas.drawLine(getX(startpoint.getXPosition()), getY(startpoint.getYPosition()), getX(endpoint.getXPosition()), getY(endpoint.getYPosition()), paint);
	}
	
	/**
	 * Action performed when displaying graph block.
	 * Sets Canvas object in properties.
	 * Runs translation & scale operations.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		this.canvas = canvas;
		this.canvas.save();
		this.canvas.translate(translationX, translationY);	// FIXME
		this.canvas.scale(scaleFactor, scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
		drawGraph(graph);
		this.canvas.restore();	// must be called AFTER drawing canvas elements
	}
	
	/**
	 * Helper to get absolute X coordinate.
	 * @param x relative X coordinate
	 * @return float value
	 */
	private float getX(double x) {
		return (float)(x * getMeasuredWidth());
	}
	
	/**
	 * Helper to get absolute Y coordinate.
	 * @param y relative Y coordinate
	 * @return float value
	 */
	private float getY(double y) {
		return (float)(y * getMeasuredHeight());
	}
	
	/**
	 * LAYOUT METHODS
	 */
	
	/**
	 * Toggles vertex labels on the graph. Invalidates the view.
	 * 
	 * @param checked is the toggle checkbox checked?
	 */
	public void toggleLabels(boolean checked) {
		showLabels = checked;
		invalidate();
	}
	
	/**
	 * SCALE EVENT LISTENERS
	 */
	
	/**
	 * Resets scale to default values.
	 */
	public void resetScale() {
		translationX = Constants.DEFAULT_TRANSLATION_X;
		translationY = Constants.DEFAULT_TRANSLATION_X;
		setScale(Constants.DEFAULT_SCALE);
	}
	
	/**
	 * Setter for scale factor. Also invalidates the view.
	 * @param factor new factor
	 */
	public void setScale(float factor) {
		scaleFactor = Math.max(Constants.MIN_SCALE, Math.min(factor, Constants.MAX_SCALE));
		invalidate();
	}
	
	/**
	 * Getter for current scale factor.
	 * @return float
	 */
	public float getScale() {
		return scaleFactor;
	}
	
	/**
	 * Standard onTouchEvent handler.
	 * It detects and manages either scale events or drag & scroll events. 
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		scaleGestureDetector.onTouchEvent(event);
		if (!scaleGestureDetector.isInProgress()) {
			dragVertexEvent(event);		// DRAG EVENT CALL
			scrollDisplayEvent(event);	// NAVIGATE INSIDE CANVAS
		}
		invalidate();
		return true;
	}
	
	/**
	 * Listener which changes the scale factor as we pinch zoom.
	 * After all, it sends information to canvas to redraw the view.
	 */
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			scaleFactor *= detector.getScaleFactor();
			setScale(scaleFactor);
			return true;
		}
	}
	
	/**
	 * Event ran when we catch or move a vertex within the graph.
	 * First it checks if tap event was close enough to any of the vertices.
	 * If there are more than one vertices in the radius - it chooses the closest one.
	 * When a drag event is detected, it just moves previously detected vertex.
	 * 
	 * @param event TouchEvent
	 * @return true
	 */
	private boolean dragVertexEvent(MotionEvent event) {
		int action = event.getAction();

		double X = (double) event.getX();
		double Y = (double) event.getY();

		switch (action) {

		case MotionEvent.ACTION_DOWN: // select a vertex to move
			draggedVertex = null;
			double minDistance = getMeasuredWidth();	// will be overriden
			int closeEnough = Constants.VERTEX_RADIUS * Constants.NEAR;	// HARDCODED empirical distance
			for (Vertex vert : getGraph().getVertexes()) {
				double vertX = getX(vert.getXPosition());
				double vertY = getY(vert.getYPosition());
				double distance = Math
						.sqrt((((vertX - X) * (vertX - X)) + (vertY - Y)
								* (vertY - Y)));
				if (distance <= closeEnough) { // close enough
					if (draggedVertex == null || distance < minDistance) { // selecting
																			// the
																			// closest
																			// vertex
						draggedVertex = vert;
						minDistance = distance;
					}
				}
			}

			break;

		case MotionEvent.ACTION_MOVE: // dragging a vertex only if it's caught
			if (draggedVertex != null) {
				draggedVertex.setXPosition(X / getMeasuredWidth());
				draggedVertex.setYPosition(Y / getMeasuredHeight());
			}
			break;

		case MotionEvent.ACTION_UP:
			// dropped vertex
			draggedVertex = null;
			break;
		}
		return true;

	}
	
	/**
	 * Event ran when we navigate inside graph canvas.
	 * @TODO make it work - it needs to translate bounds visible to the user.
	 * 
	 * @param event
	 *            TouchEvent
	 * @return true
	 */
	private boolean scrollDisplayEvent(MotionEvent event) {
		// FIXME not working properly
		/*switch (event.getAction()) {

		// we move the screen only if there's no vertex dragged
		case MotionEvent.ACTION_MOVE:
			if (draggedVertex == null) {
				translationX = event.getX() - translationX;
				translationY = event.getY() - translationY;	// remember new bounds
			}
			break;

		}*/
		return true;

	}
	
}
