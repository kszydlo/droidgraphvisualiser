package com.droidgraphvisualiser.presenter;

/**
 * @author arch
 *
 * Abstract class for keeping some static constant values.
 * Used by presenter to determine default or edge values for several factors.
 */
public abstract class Constants {
	
	/**
	 * Default graph display translation for X coordinate.
	 */
	public static final float DEFAULT_TRANSLATION_X = 0.0f;
	
	/**
	 * Default graph display translation for Y coordinate.
	 */
	public static final float DEFAULT_TRANSLATION_Y = 0.0f;
	
	/**
	 * Default display scale for graph (100%).
	 */
	public final static float DEFAULT_SCALE = 1.0f;
	
	/**
	 * Minimum allowed zoom scale for graph (10%).
	 */
	public final static float MIN_SCALE = 0.1f;
	
	/**
	 * Maximum allowed zoom scale for graph (500%).
	 */
	public final static float MAX_SCALE = 5.0f;
	
	/**
	 * Scale step when zooming using buttons instead of pinch zoom.
	 */
	public final static float SCALE_STEP = 0.1f;
	
	/**
	 * Radius of each displayed vertex circle.
	 */
	public final static int VERTEX_RADIUS = 7;
	
	/**
	 * Empirical distance between finger tap and a vertex used to detect drag event for the latter.
	 */
	public final static int NEAR = 6;
	
	/**
	 * Y-coordinate offset between vertex centre and bottom of its label.
	 */
	public final static float LABEL_OFFSET = -10.0f;
	
	/**
	 * Vertex label font height.
	 */
	public final static float LABEL_SIZE = 25.0f;

}