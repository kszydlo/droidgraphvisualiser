/**
 * 
 */
package com.droidgraphvisualiser.presenter;

import com.droidgraphvisualiser.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author arch
 *
 * Class responsible for displaying the graph itself inside view fragment.
 * See res/layout/graph_fragment.xml
 */
public class GraphFragment extends Fragment {
	
	/**
	 * Default create method for this fragment.
	 * Inflates the fragment as non-root element.
	 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
        return inflater.inflate(R.layout.graph_fragment, container, false);
    }
}
